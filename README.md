

import UIKit
import Foundation

class ViewController: UIViewController, UITextViewDelegate {

    
    @IBOutlet weak var txtView: UITextView!
    var font1: UIFont!
    let standardFont = UIFont.systemFont(ofSize: 10.0)
    
    var firstText: String!

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
       txtView.delegate = self
        font1 = standardFont
        firstText = txtView.text
        setText()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    


    
    
    
    
    func textViewDidChange(_ textView: UITextView) {
       

        
        font1 = font1 == standardFont ? UIFont.init(name: "Farah", size: 17.0): standardFont
        firstText = firstText == txtView.text ? txtView.text: txtView.text
        setText()
        
        
        
        
    }
    
    
    
    var attributedTextOne: NSMutableAttributedString {
        let str = txtView.text
      var startindex = ""
        var endindex = ""
        
        startindex = "\(String(describing: str?.index(of: "h1")))"
         endindex = "\(String(describing: str?.endIndex(of: "h1end ")))"
        
    
        
        
        print(startindex)
        print("end value is : \(endindex)")
        let attrText = NSMutableAttributedString(string: firstText)
        
        if startindex != "nil" && endindex != "nil" {
 
            

            
        attrText.addAttribute(NSFontAttributeName, value: font1, range: NSRange(location: Int(startindex)!, length: Int(endindex)!))
              return attrText
        }
        else if startindex != "nil" || endindex != "nil" {
            
            
            attrText.addAttribute(NSFontAttributeName, value: font1, range: NSRange(location: 0, length: 0))
            return attrText
        
        }
            else{

                 attrText.addAttribute(NSFontAttributeName, value: font1, range: NSRange(location: 0, length: 0))
              return attrText
        }
 
        
    }
    
    
    
    
    func setText() {
        self.txtView.attributedText = self.attributedTextOne
    }
    
    
    @IBAction func changeFont(_ sender: UIButton) {
        font1 = font1 == standardFont ? UIFont.init(name: "Farah", size: 17.0): standardFont
        firstText = firstText == txtView.text ? txtView.text: txtView.text
        setText()
    }


}






extension String {
    func index(of string: String, options: CompareOptions = .literal) -> Index? {
        return range(of: string, options: options)?.lowerBound
    }
    func endIndex(of string: String, options: CompareOptions = .literal) -> Index? {
        return range(of: string, options: options)?.upperBound
    }
    func indexes(of string: String, options: CompareOptions = .literal) -> [Index] {
        var result: [Index] = []
        var start = startIndex
        while let range = range(of: string, options: options, range: start..<endIndex) {
            result.append(range.lowerBound)
            start = range.upperBound
        }
        return result
    }
    func ranges(of string: String, options: CompareOptions = .literal) -> [Range<Index>] {
        var result: [Range<Index>] = []
        var start = startIndex
        while let range = range(of: string, options: options, range: start..<endIndex) {
            result.append(range)
            start = range.upperBound
        }
        return result
    }
}
